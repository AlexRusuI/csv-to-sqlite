package com.alex.dbx;

import com.alex.dbx.dao.SolutionDAO;
import com.alex.dbx.utils.Connect;

import java.sql.SQLException;

public class CsvToSQLite {
    public static void main(String[] args) throws SQLException {

        Connect connect = new Connect();
        SolutionDAO solutionDAO = new SolutionDAO();
        connect.openConnection();
        solutionDAO.createTable(connect);
        solutionDAO.insertFromCSV(connect);
        solutionDAO.logStatistics();
        connect.closeConnection();
    }
}
