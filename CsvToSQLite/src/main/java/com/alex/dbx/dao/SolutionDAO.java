package com.alex.dbx.dao;

import com.alex.dbx.utils.Connect;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@Getter
@ToString
@EqualsAndHashCode
public class SolutionDAO {

    private final DateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
    private final String csvPath = System.getProperty("user.home") + "/Desktop/Interview-task-data-osh.csv";
    private final String badDataPath = System.getProperty("user.home") + "/Desktop/bad-data-";
    private final String badDataExtension = ".csv";
    private final String badDataFilename = badDataPath + df.format(new Date()) + badDataExtension;

    private final String logFilePath = System.getProperty("user.home") + "/Desktop/logFile.log";

    private int recordsReceived = 0;
    private int recordsSuccessful = 0;
    private int recordsFailed = -1;

    /**
     * Method for creating the in-memory SQLite table.
     * It gets a Connection object, that is used to create a statement for creating a table if it doesn't exist already.
     *
     * @throws SQLException if a database access error occurs.
     **/
    public void createTable(Connect connect) {
        try {
            Statement statement = connect.getConnection().createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS X"
                    + "(A       TEXT,"
                    + " B       TEXT,"
                    + " C       TEXT,"
                    + " D       TEXT,"
                    + " E       TEXT,"
                    + " F       TEXT,"
                    + " G       TEXT,"
                    + " H       TEXT,"
                    + " I       TEXT,"
                    + " J       TEXT);");

        } catch (SQLException e) {
            e.getMessage();
        }
    }

    /**
     * Method gets a Connection object, that is used to create a statement for creating a table if it doesn't exist already.
     * Method reads the .csv file wit the help of a Reader object, that then is used for a CSVReader object
     * to specifically read a .csv file. We specify for the csvReader to skip reading the first line, because it has
     * only the column names. We also create a Writer object to use to write the corrupt or incomplete data
     * to a created file located on the desktop, whose name consists of bad-data + date +.csv, and has the row names
     * in the first row.
     *
     * @throws SQLException if a database access error occurs, IOException if Input output operations failed,
     *                      and CsvValidationException if information is not valid.
     **/
    public void insertFromCSV(Connect connect) {
        try {
            Reader reader = Files.newBufferedReader(Paths.get(csvPath));
            CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

            Writer writer = Files.newBufferedWriter(Paths.get(badDataFilename));

            CSVWriter csvWriter = new CSVWriter(writer,
                    CSVWriter.DEFAULT_SEPARATOR,
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            String[] headerRecord = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
            csvWriter.writeNext(headerRecord);

            PreparedStatement preparedStatement = connect.getConnection().
                    prepareStatement("INSERT INTO X(A,B,C,D,E,F,G,H,I,J)"
                            + "VALUES(?,?,?,?,?,?,?,?,?,?);");

            String[] nextRecord;
            while ((nextRecord = csvReader.readNext()) != null) {
                recordsReceived++;

                if (!Arrays.asList(nextRecord).contains("")) {
                    recordsSuccessful++;
                    for (int i = 0; i < headerRecord.length; i++) {
                        preparedStatement.setString(i + 1, nextRecord[i]);
                    }
                    preparedStatement.executeUpdate();
                } else {
                    recordsFailed++;
                    csvWriter.writeNext(nextRecord);
                }
            }
            csvWriter.close();
        } catch (SQLException | IOException | CsvValidationException e) {
            e.getMessage();
        }
    }

    /**
     * method used to query the database and print everything to make sure the data is actually being read and the connection is valid.
     * We get a Connection object for connecting to the local database, and use it to get a result set by making a select query
     * in the database. We use ResultSetMetaData to extract the information from the resultSet object,
     * and we use resultSetMetaData to get the number of columns.
     * We then go through the resultSet to print each row from the table.
     *
     * @throws SQLException if a database access error occurs.
     **/
    public void testDB(Connect connect) {
        try {
            final Statement statement = connect.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM X;");
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int numColumns = resultSetMetaData.getColumnCount();

            while (resultSet.next()) {
                for (int i = 1; i <= numColumns; i++) {
                    System.out.print(resultSet.getString(i) + " ");
                }
                System.out.println();
            }
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    /**
     * Method used to write the record statistics to the log file.
     * The try with resources segment writes the data to the file specified in the logFilePath variable.
     *
     * @throws IOException in case the file is not present, or some other problems with the writing process occur
     **/
    public void logStatistics() {
        try (FileWriter fw = new FileWriter(logFilePath)) {
            fw.write("Records Received: " + recordsReceived + "\n");
            fw.write("Records Successful: " + recordsSuccessful + "\n");
            fw.write("Records Failed: " + recordsFailed);
        } catch (IOException e) {
            e.getMessage();
        }
    }

}
