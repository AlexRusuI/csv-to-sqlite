
This repo contains a java application that consumes a .csv file, parses the data, adds valid records to a in memory SQLite database,
writes invalid records to a output csv, and logs stats to a log file. 

Conditions are as follows:
Customer X just informed us that we need to churn out a code enhancement ASAP for a new project.  Here is what they need:

 
1. We need a Java application that will consume a CSV file, parse the data and insert to a SQLite In-Memory database.  

a. Table X has 10 columns A, B, C, D, E, F, G, H, I, J which correspond with the CSV file column header names.

b. Include all DDL in submitted repository

c. Create your own SQLite DB

 
2. The data sets can be extremely large so be sure the processing is optimized with efficiency in mind.  

 
3. Each record needs to be verified to contain the right number of data elements to match the columns.  

a. Records that do not match the column count must be written to the bad-data-<timestamp>.csv file

b. Elements with commas will be double quoted

 
4. At the end of the process write statistics to a log file

a. # of records received

b. # of records successful

c. # of records failed

Setup Summary:

-Ensure the Java SDK (JDK) is installed on your computer. Use Java SE 8 to avoid any issues.
-Ensure Maven is installed on your computer. See https://maven.apache.org/.
-Ensure Git is installed on your computer. See https://git-scm.com/downloads.
-Clone the repo locally: git clone https://AlexRusuI@bitbucket.org/AlexRusuI/csv-to-sqlite.git.
-Copy the Interview-task-data-osh.csv file from the project file path to the desktop. (this step can be changed to be skipped later).
-Open a command prompt and cd into CsvToSQLite folder (make sure you're inside the folder containing the pom.xml file).
-You might have to update the compiler version in the pom.xml file (javac -version to check). 
-In your terminal, execute "mvn clean install", and after that, execute "mvn exec:java", which will make it so that all 
	bad records will be saved to a file named bad-data + date and time of program execution + .csv, and the log will
	be saved to a log file named logFile.log. For the sake of brevity, all these files will be located on the desktop,
	but it won't take much to change that path to something that suits us a little better. 

Developer notes:
In order to make this software work, I needed to connect to a SQLite database do I could make table manipulations. 
I decided to go with a temporary solution using an in memory SQLite database in order to get a connection. After that, 
in the SolutionDAO class I wrote methods that use this connection object in order to make table manipulations
by creating prepared statements. I used CSVReader and CSVWriter object by adding the opencsv dependency in the pom.xml file
in order to read the provided .csv file, and write the bad records to a bad-data- + date + .csv file on the desktop. I added
the .withSkipLines(1) method to the reader in order to not include the first row of the file, which is the column names.
I went through each record of the .csv file in a while loop, and checked if any of the row cells are empty. If positive,
the numner of bad data incremented, and the bad record would get added to the bad-data .csv file. if negative and the record is 
good, the number of good records would get increased.
In the end, the statistics are written to a logFile.log file on the desktop. Again, this path can be changed later.

There is a number of improvements to be made, like increasing performance and writing tests, but as a Demo I hope it works.
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

